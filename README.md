# Repo for first seminar "Coding for Humanities"

[Slides](https://git.informatik.uni-leipzig.de/introduction-dh/CodingForHumanities1/blob/master/HowToGit.pdf)

### 1st Assignment

* Fork the project
* Clone it to your local machine to a folder of your choice, e.g. `/GitLabProjects/`
* Produce a markdown file describing your expectations about this course that includes at least two headers of different size, a hyperlink, and bullet point list
* Save this markdown file in your local project under the subfolder `/GitLabProjects/ExpectationsStudents/`
* Add the new file to your local repository
* Commit the file to your local repository
* Push the changes to your GitLab repository
* Create a Merge Request and assign it to me: koentges
* Celebrate your first public contribution to a small digital project


#### Help

* How to [clone](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#clone-your-project) a project.
* How to [fork](https://docs.gitlab.com/ce/workflow/forking_workflow.html#creating-a-fork) and send a [merge](https://docs.gitlab.com/ce/workflow/forking_workflow.html#merging-upstream) request.
