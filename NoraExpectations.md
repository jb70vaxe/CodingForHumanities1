##First Assignment

Hi I'm Nora and I'm studying Digital Humanities in [Leipzig](https://www.leipzig-studieren.de/digital-humanities-bsc/). I just moved here because I'm actually from [Cologne](http://www.koeln.de). 



####My Expectations about Digital Humanities 

* to expand my horizons 
* to learn about things whose existence I didn't even know before
* to work together with interesting people on interesting projects
* to make new experiences
